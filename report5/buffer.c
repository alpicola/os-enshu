#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t not_empty = PTHREAD_COND_INITIALIZER; 
pthread_cond_t not_full = PTHREAD_COND_INITIALIZER;

int BB_SIZE = 10;
int bb_buf[10];
int bb_counter = 0;
int bb_get_index = 0;
int bb_put_index = 0;

int bb_get() {
    int i;

    pthread_mutex_lock(&lock);
    while (bb_counter == 0) {
        pthread_cond_wait(&not_empty, &lock);
    }
    i = bb_buf[bb_get_index];
    bb_get_index = (bb_get_index + 1) % BB_SIZE;
    bb_counter--;
    pthread_cond_signal(&not_full);
    pthread_mutex_unlock(&lock);

    return i;
};

void bb_put(int i) {
    pthread_mutex_lock(&lock);
    while (bb_counter == BB_SIZE) {
        pthread_cond_wait(&not_full, &lock);
    }
    bb_buf[bb_put_index] = i;
    bb_put_index = (bb_put_index + 1) % BB_SIZE;
    bb_counter++;
    pthread_cond_signal(&not_empty);
    pthread_mutex_unlock(&lock);
};

void *producer_thread(void *arg) {
    int i = *(int*)arg;
    bb_put(i);

    return NULL;
}

void *consumer_thread(void *arg) {
    int i = bb_get();
    printf("%d\n", i);

    return NULL;
}

int main() {
    int i;
    int N = 100;
    int data[N];
    pthread_t producers[N];
    pthread_t consumers[N];

    for (i = 0; i < N; i++) {
        data[i] = i;
        if (pthread_create(&producers[i], NULL, producer_thread, &data[i]) != 0) {
            perror("create thread");
            exit(EXIT_FAILURE);
        }
        if (pthread_create(&consumers[i], NULL, consumer_thread, NULL) != 0) {
            perror("create thread");
            exit(EXIT_FAILURE);
        }
    }

    for (i = 0; i < N; i++) {
        if (pthread_join(producers[i], NULL) != 0) {
            perror("join thread");
            exit(EXIT_FAILURE);
        }
        if (pthread_join(consumers[i], NULL) != 0) {
            perror("join thread");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
