#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct _tnode {
    int value;
    struct _tnode *left;
    struct _tnode *right;
} tnode;

tnode *tree_insert(int i, tnode *t) {
    if (t == NULL) {
        t = (tnode*)malloc(sizeof(tnode));
        t->value = i;
        t->left = NULL;
        t->right = NULL;
    } else {
        if (t->value < i) {
            t->left = tree_insert(i, t->left);
        } else {
            t->right = tree_insert(i, t->right);
        }
    }

    return t;
}

int tree_size(tnode *t) {
    if (t == NULL) {
        return 0;
    } else {
        return 1 + tree_size(t->left) + tree_size(t->right);
    }
}

tnode *tree;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int N = 100;

void *insert_thread(void *arg) {
    pthread_mutex_lock(&mutex);
    tree = tree_insert(1, tree);
    pthread_mutex_unlock(&mutex);
    return arg;
}

int main() {
    int i = 0;
    pthread_t threads[N];

    for (i = 0; i < N; i++) {
        if (pthread_create(&threads[i], NULL, insert_thread, NULL) != 0) {
            perror("create thread");
            exit(EXIT_FAILURE);
        }
    }

    for (i = 0; i < N; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            perror("join thread");
            exit(EXIT_FAILURE);
        }
    }

    printf("number of threads: %d\n", N);
    printf("tree size: %d\n", tree_size(tree));

    return 0;
}
