#include <linux/linkage.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

asmlinkage long sys_flip(char __user *p, long len) {
    int i;
    char c;
    char *s;

    s = (char *) kmalloc(len, GFP_KERNEL);
    if (s == NULL) {
        return 0;
    }
    if (copy_from_user(s, p, len)) {
        return 0;
    }

    for (i = 0; i < len / 2; i++) {
        c = s[i];
        s[i] = s[len-i-1];
        s[len-i-1] = c;
    }

    if (copy_to_user(p, s, len)) {
        return 0;
    }
    kfree(s);

    return len;
}
