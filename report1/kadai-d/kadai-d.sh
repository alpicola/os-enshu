#!/bin/sh

for i in *.cpp; do
    mv $i ${i%.cpp}.cc
done

sed -i "s/NEET the 3rd/田中諒/" *.cc
sed -i "s/neet3@example.com/alpicola@is.s.u-tokyo.ac.jp/" *.cc
