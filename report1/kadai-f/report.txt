実行環境:
  Ubuntu 12.04 LTS, zsh
実行例:
  % gcc loop.c -o loop
  % ./loop &
  % top
  top - 17:28:37 up  1:52,  3 users,  load average: 0.16, 0.14, 0.08
  Tasks: 177 total,   2 running, 174 sleeping,   0 stopped,   1 zombie
  Cpu(s):  1.2%us,  0.4%sy,  0.2%ni, 97.9%id,  0.3%wa,  0.0%hi,  0.1%si,  0.0%st
  Mem:   3844428k total,  1487636k used,  2356792k free,    66384k buffers
  Swap:  3879932k total,        0k used,  3879932k free,   746424k cached

   PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
  4616 ryo       25   5  4156  356  276 R   99  0.0   0:02.87 loop
  1344 root      20   0  140m  23m 5372 S    2  0.6   2:09.37 Xorg
  ...
  % kill 4616
