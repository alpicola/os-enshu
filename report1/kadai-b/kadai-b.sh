#!/bin/sh

find ../coreutils-8.9/src -name '*.c' | xargs wc -l | head -n -1 | sort | awk '{print $2}' > result.txt
