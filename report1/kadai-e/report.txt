実行環境:
  Ubuntu 12.04 LTS, zsh
実行例:
  (catの出力をresult1.txtに保存)
  % strace /bin/cat /proc/meminfo > result1.txt
  (straceの出力をresult2.txtに保存)
  % strace /bin/cat /proc/meminfo 2> result2.txt
  (catとstraceの出力をresult.txtに保存)
  % strace /bin/cat /proc/meminfo &> result.txt
