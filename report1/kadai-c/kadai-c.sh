#!/bin/zsh

URL=http://report.il.is.s.u-tokyo.ac.jp/os2013-resume/kadai1/1.pdf

printf "$URL.%02d\n" {0..99} | xargs -P 5 -n 1 wget

cat 1.pdf.* > 1.pdf
