#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int sock;
    struct sockaddr_in addr;
    unsigned short port;
    char host[80];
    struct hostent *hp;

    int BUF_SIZE = 2048;
    char buf[BUF_SIZE];
    int read_size, write_size;
    long long int total_size;

    struct timeval start, end;
    struct timeval t1, t2;
    int c, N;
    float elapsed, latency;
    float throughput;

    if (argc != 3) {
        fputs("usage: bwclient host port\n", stderr);
        exit(EXIT_FAILURE);
    }
    sscanf(argv[1], "%s", host);
    sscanf(argv[2], "%hu", &port);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    hp = gethostbyname(host);
    if (hp == NULL) {
        fputs("invalid host\n", stderr);
        exit(EXIT_FAILURE);
    }
    memcpy(&(addr.sin_addr.s_addr), hp->h_addr, hp->h_length);

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("connect");
        exit(EXIT_FAILURE);
    }

    c = N = 128;
    gettimeofday(&start, NULL);

    while (1) {
        write_size = write(sock, buf, BUF_SIZE);
        if (write_size == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }

        if (--c == 0) {
            gettimeofday(&end, NULL);
            timersub(&end, &start, &t1);
            if (t1.tv_sec >= 10) {
                elapsed = t1.tv_sec + t1.tv_usec * 1e-6;
                break;
            } else {
                c = N;
            }
        }
    }

    shutdown(sock, SHUT_WR);
    read_size = read(sock, buf, BUF_SIZE);
    if (read_size == -1) {
        perror("read");
        exit(EXIT_FAILURE);
    }
    gettimeofday(&t1, NULL);

    close(sock);

    timersub(&t1, &end, &t2);
    latency = (t2.tv_sec + t2.tv_usec * 1e-6) / 2;
    sscanf(buf, "%lld", &total_size);
    throughput =  total_size * 8 / elapsed * 1e-6;
    printf("%lld\t%.2f\t%.2f\n", total_size, elapsed, throughput);
    printf("%.2f\n", latency);

    return 0;
}
