#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

void sighandler(int signum) {
   while (waitpid(-1, NULL, WNOHANG) > 0) {}
}

int main(int argc, char *argv[]) {
    int sock, conn_sock;
    unsigned short port;
    struct sockaddr_in addr;

    int BUF_SIZE = 2048;
    char buf[BUF_SIZE];
    int read_size, write_size;

    struct sigaction sigact;

    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = SA_RESTART;
    sigact.sa_handler = sighandler;

    sigaction(SIGCHLD, &sigact, NULL);

    if (argc != 2) {
        fputs("usage: tcpechoserver port\n", stderr);
        exit(EXIT_FAILURE);
    }
    sscanf(argv[1], "%hu", &port);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    
    if (listen(sock, 10) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    
    while (1) {
        conn_sock = accept(sock, NULL, NULL);

        if (fork() == 0) {
            close(sock);

            while (1) {
                read_size = read(conn_sock, buf, BUF_SIZE);
                if (read_size == -1) {
                    perror("read");
                    exit(EXIT_FAILURE);
                } else if (read_size == 0) {
                    break;
                } else {
                    write_size = write(conn_sock, buf, read_size);
                    if (write_size == -1) {
                        perror("write");
                        exit(EXIT_FAILURE);
                    }
                }
            }

            close(conn_sock);
            exit(EXIT_SUCCESS);
        } else {
            close(conn_sock);
        }
    }

    close(sock);

    return 0;
}
