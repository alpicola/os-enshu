#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <poll.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    int i;
    int sock, conn_sock;
    unsigned short port;
    struct sockaddr_in addr;

    int MAX_CONNECTION = 128;
    struct pollfd pfd[MAX_CONNECTION];

    int BUF_SIZE = 2048;
    char buf[BUF_SIZE];
    int read_size, write_size;

    if (argc != 2) {
        fputs("usage: tcpechoserver port\n", stderr);
        exit(EXIT_FAILURE);
    }
    sscanf(argv[1], "%hu", &port);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    
    if (listen(sock, 10) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    pfd[0].fd = sock;
    pfd[0].events = POLLIN;
    for (i = 1; i < MAX_CONNECTION; i++) {
        pfd[i].fd = -1;
        pfd[i].events = POLLIN;
    }
    
    while (1) {
        if (poll(pfd, MAX_CONNECTION, -1) == -1) {
            perror("poll");
            exit(EXIT_FAILURE);
        }

        if (pfd[0].revents & POLLIN) {
            for (i = 1; i < MAX_CONNECTION; i++) {
                if (pfd[i].fd == -1) break;
            }

            if (i != MAX_CONNECTION) {
                if ((conn_sock = accept(sock, NULL, NULL)) == -1) {
                    perror("accept");
                    exit(EXIT_FAILURE);
                }
                pfd[i].fd = conn_sock;
            }
        }

        for (i = 1; i < MAX_CONNECTION; i++) {
            if (pfd[i].fd == -1) continue;

            if (pfd[i].revents & POLLIN) {
                read_size = read(pfd[i].fd, buf, BUF_SIZE);
                if (read_size == -1) {
                    perror("read");
                    exit(EXIT_FAILURE);
                } else if (read_size == 0) {
                    if (shutdown(pfd[i].fd, 1) == -1) {
                        perror("shutdown");
                        exit(EXIT_FAILURE);
                    }
                    pfd[i].fd = -1;
                } else {
                    write_size = write(pfd[i].fd, buf, read_size);
                    if (write_size == -1) {
                        perror("write");
                        exit(EXIT_FAILURE);
                    }
                }
            }

            if (pfd[i].revents & POLLHUP) {
                pfd[i].fd = -1;
            }
        }

    }

    close(sock);

    return 0;
} 
