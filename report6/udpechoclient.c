#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int s, n;
    unsigned short port;
    struct sockaddr_in addr;
    char buf[256];
    char host[80];
    struct hostent *hp;

    if (argc != 3) {
        fputs("usage: udpechocleint hostname port\n", stderr);
        exit(EXIT_FAILURE);
    }
    sscanf(argv[1], "%s", host);
    sscanf(argv[2], "%hu", &port);

    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    hp = gethostbyname(host);
    if (hp == NULL) {
        fputs("invalid host\n", stderr);
        exit(EXIT_FAILURE);
    }
    memcpy(&(addr.sin_addr.s_addr), hp->h_addr, hp->h_length);

    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        n = sendto(s, buf, strlen(buf)+1, 0, (struct sockaddr *)&addr, sizeof(addr));
        if (n == -1) {
            perror("sendto");
            exit(EXIT_FAILURE);
        }

        n = recvfrom(s, buf, sizeof(buf), 0, NULL, NULL);
        if (n == -1) {
            perror("recvfrom");
            exit(EXIT_FAILURE);
        }
        printf("%s", buf);
    }

    close(s);

    return 0;
}
