#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    int s, n;
    unsigned short port;
    struct sockaddr_in addr1;
    struct sockaddr_in addr2;
    socklen_t addrlen;
    char buf[256];

    if (argc != 2) {
        fputs("usage: udpechoserver port\n", stderr);
        exit(EXIT_FAILURE);
    }
    sscanf(argv[1], "%hu", &port);

    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    addr1.sin_family = AF_INET;
    addr1.sin_port = htons(port);
    addr1.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(s, (struct sockaddr *)&addr1, sizeof(addr1)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    
    while (1) {
        n = recvfrom(s, buf, sizeof(buf), 0, (struct sockaddr *)&addr2, &addrlen);
        if (n == -1) {
            perror("recvfrom");
            exit(EXIT_FAILURE);
        }

        n = sendto(s, buf, n, 0, (struct sockaddr *)&addr2, addrlen);
        if (n == -1) {
            perror("sendto");
            exit(EXIT_FAILURE);
        }
    }

    close(s);

    return 0;
}
