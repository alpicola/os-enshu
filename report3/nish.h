#define ARGV_SIZE 32
#define REDIR_NUM 3

typedef enum _redirection_mode {
    R_READ, R_WRITE, R_APPEND
} redirection_mode;

typedef struct _redirection {
    enum _redirection_mode mode;
    char *filepath;
} redirection;

typedef struct _command {
    int argc;
    char **argv;
    struct _redirection **redirections;
    struct _command *next;
} command;

typedef enum _token {
    T_EOF, T_EOL, T_PIPE, T_IREDIR, T_OREDIR, T_AREDIR, T_WORD
} token;
