#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nish.h"

redirection *init_redirection() {
    redirection *r;

    r = (redirection*)malloc(sizeof(redirection));
    r->mode = R_READ;
    r->filepath = NULL;

    return r;
}

void free_redirection(redirection *r) {
    free(r->filepath);
    free(r);
}

command *init_command() {
    command *c;
    int i;

    c = (command*)malloc(sizeof(command));
    c->next = NULL;

    c->argc = 0;
    c->argv = (char**)malloc(sizeof(char*) * ARGV_SIZE);
    for (i = 0; i < ARGV_SIZE; i++) {
        c->argv[i] = NULL;
    }

    c->redirections = (redirection**)malloc(sizeof(redirection*) * REDIR_NUM);
    for (i = 0; i < REDIR_NUM; i++) {
        c->redirections[i] = NULL;
    }

    return c;
}

void free_command(command *c) {
    int i;

    for (i = 0; i < c->argc; i++) {
        free(c->argv[i]);
    }

    for (i = 0; i < REDIR_NUM; i++) {
        if (c->redirections[i]) {
            free_redirection(c->redirections[i]);
        }
    }

    if (c->next != NULL) free_command(c->next);
    free(c);
}

token read_token(char *buf, int size) {
    token t;
    char c, d;
    int i = 0;
    int delimited = 0;

    while ((c = getchar()) != EOF) {
        if (c != '\t' && c != ' ') break;
    }

    switch (c) {
        case EOF:
            t = T_EOF;
            break;
        case '\n':
            t = T_EOL;
            break;
        case '\'':
            while ((c = getchar()) != EOF) {
                if (c == '\n') printf("quote> ");
                if (c == '\'') break;
                buf[i++] = c;
            }
            t =  T_WORD;
            break;
        case '|':
            buf[i++] = c;
            t = T_PIPE;
            break;
        case '>':
            buf[i++] = c;
            if ((c = getchar()) == '>') {
                buf[i++] = c;
                t = T_AREDIR;
            } else {
                ungetc(c, stdin);
                t = T_OREDIR;
            }
            break;
        case '<':
            buf[i++] = c;
            t = T_IREDIR;
            break;
        default:
            buf[i++] = c;
            t = T_WORD;

            while (!delimited && (c = getchar()) != EOF) {
                switch (c) {
                    case '\t':
                    case ' ':
                        delimited = 1;
                        break;
                    case '\n':
                    case '\'':
                    case '|':
                    case '<':
                        ungetc(c, stdin);
                        delimited = 1;
                        break;
                    case '>':
                        if (i == 1 && sscanf(buf, "%[012]", &d) == 1) {
                            buf[i++] = c;
                            if ((c = getchar()) == '>') {
                                buf[i++] = c;
                                t = T_OREDIR;
                            } else {
                                ungetc(c, stdin);
                                t = T_AREDIR;
                            }
                        } else {
                            ungetc(c, stdin);
                        }
                        delimited = 1;
                        break;
                    default:
                        buf[i++] = c;
                }
            }
    }

    buf[i] = '\0';

    return t;
}

command *read_command() {
    char buf[256];
    token t;
    command *command, *c;
    redirection *r;
    char *err;
    int fd;

    command = c = NULL;
    err = NULL;
    do {
        t = read_token(buf, 256);

        switch (t) {
            case T_EOF:
                command = init_command();
                command->argv[0] = strdup("exit");
                command->argc++;
                break;
            case T_EOL:
                if (c->argc > 0) {
                    t = T_EOF;
                } else {
                    printf("pipe> ");
                }
                break;
            case T_PIPE:
                if (c != NULL && c->argc > 0) {
                    c->next = init_command();
                    c = c->next;
                } else {
                    err = "invalid pipeline";
                }
                break;
            case T_IREDIR:
            case T_OREDIR:
            case T_AREDIR:
                fd = -1;
                sscanf(buf, "%d", &fd);

                if (c != NULL && read_token(buf, 256) == T_WORD) {
                    r = init_redirection();
                    switch (t) {
                        case T_IREDIR:
                            r->mode = R_READ;
                            if (fd < 0) fd = 0;
                            break;
                        case T_OREDIR:
                            r->mode = R_WRITE;
                            if (fd < 0) fd = 1;
                            break;
                        case T_AREDIR:
                            r->mode = R_APPEND;
                            if (fd < 0) fd = 1;
                            break;
                    }
                    c->redirections[fd] = r;
                    r->filepath = strdup(buf);
                } else {
                    err = "invalid redirection";
                }
                break;
            case T_WORD:
                if (c == NULL) {
                    c = init_command();
                    if (command == NULL) {
                        command = c;
                    }
                }
                if (c->argc == ARGV_SIZE) {
                    err = "too many arguments";
                } else {
                    c->argv[c->argc++] = strdup(buf);
                }
                break;
        }
    } while (t != T_EOF && err == NULL);

    if (err != NULL) {
        fgets(buf, 256, stdin);
        fprintf(stderr, "nish: %s\n", err);
        if (command != NULL) {
            free_command(command);
            command = NULL;
        }
    }

    return command;
}

void run_command(command *c, char *envp[]) {
    int orig;
    int fd[2];
    int i, status;
    pid_t pid;
    char prog[256], *path, *dir;

    if (strcmp(c->argv[0], "exit") == 0) {
        exit(EXIT_SUCCESS);
    }

    if (c->next != NULL) {
        if (pipe(fd) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
    }

    if ((pid = fork()) == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        if (c->next != NULL) {
            close(fd[0]);
            close(1);
            dup2(fd[1], 1);
            close(fd[1]);
        }

        for (i = 0; i < REDIR_NUM; i++) {
            redirection *r;
            int f;
            if ((r = c->redirections[i]) != NULL) {
                switch (r->mode) {
                    case R_READ:
                        f = open(r->filepath, O_RDONLY);
                        break;
                    case R_WRITE:
                        f = creat(r->filepath, S_IREAD | S_IWRITE);
                        break;
                    case R_APPEND:
                        f = open(r->filepath, O_WRONLY | O_APPEND);
                        break;
                }
                if (f < 0) {
                    perror(r->filepath);
                    exit(EXIT_FAILURE);
                }
                dup2(f, i);
            }
        }

        if (strchr(c->argv[0], '/') == NULL) {
             for (i = 0; envp[i]; i++) {
                if (strncmp(envp[i], "PATH=", 5) == 0) {
                    path = strdup(envp[i] + 5);
                    break;
                }
            }

            while ((dir = strsep(&path, ":")) != NULL) {
                int len = strlen(dir);
                strcpy(prog, dir);
                prog[len] = '/';
                strcpy(prog + len + 1, c->argv[0]);
                execve(prog, c->argv, envp);
            }
        } else {
            execve(c->argv[0], c->argv, envp);
        }

        fprintf(stderr, "nish: cannot run: %s\n", c->argv[0]);
        exit(127);
    } else {
        if (c->next != NULL) {
            orig = dup(0);
            close(0);
            dup2(fd[0], 0);
            close(fd[0]);
            close(fd[1]);

            run_command(c->next, envp);

            close(0);
            dup2(orig, 0);
            close(orig);
        }

        waitpid(pid, &status, WUNTRACED);
    }
}

void print_command(command *c) {
    int i;

    printf("command: \n");
    for (i = 0; i < c->argc; i++) {
        printf("  argv %d: %s\n", i, c->argv[i]);
    }
    for (i = 0; i < 3; i++) {
        if (c->redirections[i] != NULL) {
            printf("  redirection %d:\n", i);
            printf("    filepath: %s\n", c->redirections[i]->filepath);
            switch (c->redirections[i]->mode) {
                case R_READ: 
                    printf("    mode: read\n");
                    break;
                case R_WRITE:
                    printf("    mode: write\n");
                    break;
                case R_APPEND:
                    printf("    mode: append\n");
                    break;
            }
        }
    }

    if (c->next != NULL) {
        print_command(c->next);
    }
}

int main(int argc, char *argv[], char *envp[]) {
    while (1) {
        command *c;

        printf("> ");
        if ((c = read_command()) == NULL) {
            continue;
        }

        run_command(c, envp);
        free_command(c);
    }

    return 0;
}
