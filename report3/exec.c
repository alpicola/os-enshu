#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *envp[]) {
    pid_t pid;
    int status;
    char *path;

    if ((pid = fork()) == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        printf("child-pid = %d\n", getpid());

        argv++;
        path = *argv;
        execve(path, argv, envp);
        perror("execve");
        exit(EXIT_FAILURE);
    } else {
        printf("parent-pid = %d\n", getpid());

        if (waitpid(pid, &status, WUNTRACED) == -1) {
            perror("waitpid");
            exit(EXIT_FAILURE);
        }
        printf("status %d\n", status);
    }

    return 0;
}
