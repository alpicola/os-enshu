#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

void compare_exchange_low(int n);
void compare_exchange_high(int n);
int compare(const void *a, const void *b);

int processes_num;
int process_rank;
int array_size;
int *array;
int *send_buffer;
int *recv_buffer;

int main(int argc, char *argv[]) {
    int i, j, dim;
    int total_size;
    char *in, *out;
    double local_start, local_elapsed, elapsed;
    MPI_File fh;
    MPI_Status st;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &processes_num);

    if (argc != 4) {
        if (process_rank == 0) {
            fputs("usage: mpirun bitonic input_file output_file N\n", stderr);
        }
        exit(EXIT_FAILURE);
    }

    in = argv[1];
    out = argv[2];
    total_size = 1 << atoi(argv[3]);
    array_size = total_size / processes_num;
    dim = (int) log2(processes_num);

    array = (int *) malloc(array_size * sizeof(int));
    send_buffer = (int *) malloc(array_size * sizeof(int));
    recv_buffer = (int *) malloc(array_size * sizeof(int));

    MPI_File_open(MPI_COMM_WORLD, in, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
    MPI_File_set_view(fh, 0, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
    MPI_File_read_at_all(fh, array_size * process_rank, array, array_size, MPI_INT, &st);
    MPI_File_close(&fh);

    local_start = MPI_Wtime();

    qsort(array, array_size, sizeof(int), compare);

    for (i = 0; i < dim; i++) {
        for (j = i; j >= 0; j--) {
            if ((process_rank >> (i + 1)) % 2 == (process_rank >> j) % 2) {
                compare_exchange_low(j);
            } else {
                compare_exchange_high(j);
            }
        }
    }

    local_elapsed = MPI_Wtime() - local_start;

    MPI_Reduce(&local_elapsed, &elapsed, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    if (process_rank == 0) {
        printf("elapsed: %.10f sec\n", elapsed);
    }

    MPI_File_open(MPI_COMM_WORLD, out, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fh);
    MPI_File_set_view(fh, 0, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
    MPI_File_write_at_all(fh, array_size * process_rank, array, array_size, MPI_INT, &st);
    MPI_File_close(&fh);

    free(array);
    free(send_buffer);
    free(recv_buffer);

    MPI_Finalize();

    return 0;
}

void compare_exchange_low(int n) {
    int i, j, k;
    MPI_Status st;

    memcpy(send_buffer, array, array_size * sizeof(int));

    MPI_Sendrecv(send_buffer, array_size, MPI_INT, process_rank ^ (1 << n), n,
                 recv_buffer, array_size, MPI_INT, process_rank ^ (1 << n), n, MPI_COMM_WORLD, &st);

    i = j = k = 0;
    while (k < array_size) {
        if (send_buffer[i] < recv_buffer[j]) {
            array[k++] = send_buffer[i++];
        } else {
            array[k++] = recv_buffer[j++];
        }
    }
}

void compare_exchange_high(int n) {
    int i, j, k;
    MPI_Status st;

    memcpy(send_buffer, array, array_size * sizeof(int));

    MPI_Sendrecv(send_buffer, array_size, MPI_INT, process_rank ^ (1 << n), n,
                 recv_buffer, array_size, MPI_INT, process_rank ^ (1 << n), n, MPI_COMM_WORLD, &st);

    i = j = k = array_size - 1;
    while (k >= 0) {
        if (send_buffer[i] > recv_buffer[j]) {
            array[k--] = send_buffer[i--];
        } else {
            array[k--] = recv_buffer[j--];
        }
    }
}

int compare(const void *a, const void *b) {
    if (*(int *) a < *(int *) b) return -1;
    if (*(int *) a == *(int *) b) return 0;
    return 1;
}
