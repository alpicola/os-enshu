#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <time.h>

int BUF_SIZE = 4096;

typedef enum {
    GET, HEAD, OTHER
} method_t;

void sighandler(int signum) {
   while (waitpid(-1, NULL, WNOHANG) > 0) {}
}

int recv_request(int f, method_t *method, char *path) {
    char buf[BUF_SIZE];
    int read_size;
    char meth[8];
    char *line, *bp;

    read_size = recv(f, buf, BUF_SIZE, 0);
    buf[read_size] = '\0';
    bp = buf;

    // request line
    if ((line = strsep(&bp, "\n")) != NULL) {
        if (sscanf(line, "%7s %255s HTTP/1.%*d\r", meth, path) == 2) {
            if (path[0] != '/' || strstr(path, "/../") != NULL) {
                return -1;
            }
            if (strcmp(meth, "GET") == 0) {
                *method = GET;
            } else if (strcmp(meth, "HEAD") == 0) {
                *method = HEAD;
            } else {
                *method = OTHER;
            }
        } else {
            return -1;
        }
    } else {
        return -1;
    }

    // ignore headers
    while ((line = strsep(&bp, "\n")) != NULL) {
        if (strcmp(line, "\r") == 0) break;
    }

    return 0;
}

int send_response(int f, int status, int length, char *type) {
    char buf[256];
    char *reason = "";
    time_t t;
    char *date;

    switch (status) {
        case 200: reason = "OK"; break;
        case 400: reason = "Bad Request"; break;
        case 404: reason = "Not Found"; break;
        case 501: reason = "Not Implemented"; break;
    }
    if (status != 200) {
        length = strlen(reason) + 4;
        type = "text/plain";
    }

    sprintf(buf, "HTTP/1.1 %d %s\r\n", status, reason);
    write(f, buf, strlen(buf));

    time(&t);
    date = ctime(&t);
    date[strlen(date)-1] = '\0';
    sprintf(buf, "Date: %s\r\n", date);
    write(f, buf, strlen(buf));

    if (length >= 0) {
        sprintf(buf, "Content-Length: %d\r\n", length);
        write(f, buf, strlen(buf));
    }

    if (type != NULL) {
        sprintf(buf, "Content-Type: %s\r\n", type);
        write(f, buf, strlen(buf));
    }

    write(f, "\r\n", 2);

    if (status != 200) {
        sprintf(buf, "%d %s", status, reason);
        write(f, buf, strlen(buf));
    }

    return 0;
}

int main(int argc, char *argv[]) {
    int sock, conn_sock, f, on;
    unsigned short port;
    struct sockaddr_in addr;

    char buf[BUF_SIZE];
    int read_size, write_size;
    struct stat st;
    method_t method;
    char path[256];
    char *pathp, *type, *ext;
    char root[256];

    struct sigaction sigact;

    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = SA_RESTART;
    sigact.sa_handler = sighandler;

    sigaction(SIGCHLD, &sigact, NULL);

    if (argc != 3) {
        fputs("usage: httpserver port root\n", stderr);
        exit(EXIT_FAILURE);
    }
    sscanf(argv[1], "%hu", &port);
    sscanf(argv[2], "%s", root);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    on = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    
    if (listen(sock, 10) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    
    while (1) {
        conn_sock = accept(sock, NULL, NULL);

        if (fork() == 0) {
            close(sock);

            if (recv_request(conn_sock, &method, path) == -1) {
                send_response(conn_sock, 400, -1, NULL);
                close(conn_sock);
                exit(EXIT_SUCCESS);
            }

            if (method == OTHER) {
                send_response(conn_sock, 501, -1, NULL);
                close(conn_sock);
                exit(EXIT_SUCCESS);
            }

            pathp = path;
            pathp++;
            if (stat(pathp, &st)  == -1 || !S_ISREG(st.st_mode)) {
                perror("stat");
                send_response(conn_sock, 404, -1, NULL);
                close(conn_sock);
                exit(EXIT_SUCCESS);
            }

            type = "text/plain";
            if ((ext = strrchr(path, '.')) != NULL) {
                if (strcmp(ext+1, "html") == 0) {
                    type = "text/html";
                }
            }

            send_response(conn_sock, 200, st.st_size, type);

            if (method == HEAD) {
                close(conn_sock);
                exit(EXIT_SUCCESS);
            }

            if ((f = open(pathp, O_RDONLY)) == -1) {
                perror("open");
                exit(EXIT_FAILURE);
            }

            while (1) {
                read_size = read(f, buf, BUF_SIZE);
                if (read_size == -1) {
                    perror("read");
                    exit(EXIT_FAILURE);
                } else if (read_size == 0) {
                    break;
                } else {
                    char *bp = buf;
                    while (1) {
                        write_size = write(conn_sock, bp, read_size);
                        if (write_size == -1) {
                            perror("write");
                            exit(EXIT_FAILURE);
                        } else if (write_size == read_size) {
                            break;
                        } else {
                            read_size -= write_size;
                            bp += write_size;
                        }
                    }
                }
            }

            close(f);
            close(conn_sock);
            exit(EXIT_SUCCESS);
        } else {
            close(conn_sock);
        }
    }

    close(sock);

    return 0;
}
