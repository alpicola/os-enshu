#include <stdio.h>
#include <sys/time.h>

int main(void) {
    struct timeval t1;
    struct timeval t2;
    int elapsed = 0;

    int i, N = 1000000;
    for (i = 0; i < N; i++) {
        gettimeofday(&t1, NULL);
        gettimeofday(&t2, NULL);
        elapsed += t2.tv_usec - t1.tv_usec;
    }
    printf("%f\n", ((double)elapsed) / N);
    
    return 0;
}
