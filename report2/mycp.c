#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int BUF_SIZE = 256;

void cp_syscall(char *f1, char *f2) {
    int src;
    int dst;

    src = open(f1, O_RDONLY);
    if (src < 0) {
        perror(f1);
        exit(EXIT_FAILURE);
    }
    dst = creat(f2, S_IREAD | S_IWRITE);
    if (dst < 0) {
        perror(f2);
        exit(EXIT_FAILURE);
    }

    while (1) {
        char buf[BUF_SIZE];
        int read_size, write_size;

        read_size = read(src, buf, BUF_SIZE);
        if (read_size == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        } else if (read_size == 0) {
            break;
        } else {
            while (1) {
                write_size = write(dst, buf, read_size);
                if (write_size == -1) {
                    perror("write");
                    exit(EXIT_FAILURE);
                } else if (write_size == read_size) {
                    break;
                } else {
                    read_size -= write_size;
                }
            }
        }
    }

    if (close(src) != 0) {
        perror("close");
        exit(EXIT_FAILURE);
    }
    if (close(dst) != 0) {
        perror("close");
        exit(EXIT_FAILURE);
    }
}

void cp_stdio(char *f1, char *f2) {
    FILE *src;
    FILE *dst;

    src = fopen(f1, "r");
    if (src == NULL) {
        perror(f1);
        exit(EXIT_FAILURE);
    }
    dst = fopen(f2, "w");
    if (dst == NULL) {
        perror(f2);
        exit(EXIT_FAILURE);
    }

    while (1) {
        char buf[BUF_SIZE];
        int read_size, write_size;

        read_size = fread(buf, sizeof(char), BUF_SIZE, src);
        if (read_size == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        } else if (read_size == 0) {
            break;
        } else {
            while (1) {
                write_size = fwrite(buf, sizeof(char), read_size, dst);
                if (write_size == -1) {
                    perror("write");
                    exit(EXIT_FAILURE);
                } else if (write_size == read_size) {
                    break;
                } else {
                    read_size -= write_size;
                }
            }
        }
    }

    if (fclose(src) != 0) {
        perror("close");
        exit(EXIT_FAILURE);
    }
    if (fclose(dst) != 0) {
        perror("close");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    int opt;
    int flags = 0;

    while ((opt = getopt(argc, argv, "b:ft")) != -1) {
        switch (opt) {
            case 'b':
                sscanf(optarg, "%d", &BUF_SIZE);
                break;
            case 'f':
                flags |= 1;
                break;
            case 't':
                flags |= 2;
                break;
        }
    }
    argc -= optind;
    argv += optind;

    if ((flags >> 1) & 1) {
        struct timeval t1;
        struct timeval t2;

        gettimeofday(&t1, NULL);
        if (flags & 1) {
            cp_stdio(argv[0], argv[1]);
        } else {
            cp_syscall(argv[0], argv[1]);
        }
        gettimeofday(&t2, NULL);

        printf("%ld msec\n", t2.tv_usec - t1.tv_usec);
    } else {
        if (flags & 1) {
            cp_stdio(argv[1], argv[2]);
        } else {
            cp_syscall(argv[1], argv[2]);
        }
    }

    return 0;
}
