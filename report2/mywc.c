#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int BUF_SIZE = 4096;

void wc(char *f) {
    int lines_num, words_num, bytes_num;
    int in_line, in_word;
    int src;

    src = open(f, O_RDONLY);
    if (src < 0) {
        perror(f);
        exit(EXIT_FAILURE);
    }

    lines_num = words_num = bytes_num = 0;
    in_word = 0;

    while (1) {
        char buf[BUF_SIZE];
        int read_size, write_size;

        read_size = read(src, buf, BUF_SIZE);
        if (read_size == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        } else if (read_size == 0) {
            break;
        } else {
            bytes_num += read_size;

            int i;
            for (i = 0; i < read_size; i++) {
                switch (buf[i]) {
                    case ' ':
                        in_word = 0;
                        break;
                    case '\n':
                        in_word = 0;
                        lines_num++;
                        break;
                    default:
                        if (!in_word) {
                            words_num++;
                            in_word = 1;
                        }

                }
            }
        }
    }

    printf("%d\t%d\t%d\t%s\n", lines_num, words_num, bytes_num, f);

    if (close(src) != 0) {
        perror("close");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    wc(argv[1]);

    return 0;
}
