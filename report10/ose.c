#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/if_ether.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/spinlock.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/semaphore.h>
#include <asm/uaccess.h>

static int OSE_MAJOR = 240;

static int access_ok;
static spinlock_t lock, receive_queue_lock;
static unsigned char daddr[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
static struct sk_buff_head receive_queue;
static wait_queue_head_t read_queue;

static int ose_open(struct inode *inode, struct file *filp) {
    spin_lock(&lock);
    if (access_ok) {
        access_ok = 0;
        spin_unlock(&lock);
        return 0;
    } else {
        spin_unlock(&lock);
        return -EBUSY;
    }
}

static int ose_release(struct inode *inode, struct file *filp) {
    spin_lock(&lock);
    access_ok = 1;
    spin_unlock(&lock);
    return 0;
}

static ssize_t ose_read(struct file *filp, char __user *buf,
                    size_t count , loff_t *pos) {
    int len, c;
    struct sk_buff *skb;

    spin_lock_bh(&receive_queue_lock);

    while (skb_queue_empty(&receive_queue)) {
        spin_unlock_bh(&receive_queue_lock);
        
        if (wait_event_interruptible(read_queue, !skb_queue_empty(&receive_queue))) {
            return -EINTR;
        }

        spin_lock_bh(&receive_queue_lock);
    }

    c = count;
    do {
        skb = skb_peek(&receive_queue);
        if (c < skb->len) {
            len = c;
            copy_to_user(buf, skb->data, len);
            buf += len;
            skb_pull(skb, len);
            c = 0;
        } else {
            skb = skb_dequeue(&receive_queue);
            len = skb->len;
            copy_to_user(buf, skb->data, len);
            buf += len;
            dev_kfree_skb(skb);
            c -= len;
        }
    } while (c > 0 && !skb_queue_empty(&receive_queue));

    spin_unlock_bh(&receive_queue_lock);

    *pos += count - c;

    return count - c;
}

static ssize_t ose_write(struct file *filp, const char __user *buf,
                     size_t count , loff_t *pos) {
    int len, max_len, c, ret;
    unsigned char *p;
    struct net_device *dev;
    struct sk_buff *skb;

    dev = dev_get_by_name(&init_net, "eth0");
    max_len = dev->mtu - dev->hard_header_len - 2;

    c = count;
    do {
        len = c > max_len ? max_len : c; 
        c -= len;

        skb = alloc_skb(len + dev->hard_header_len + 2, GFP_KERNEL);
        skb->dev = dev;
        skb->protocol = htons(ETH_P_X25);

        skb_reserve(skb, dev->hard_header_len);
        if (dev_hard_header(skb, dev, ETH_P_X25, daddr, dev->dev_addr, dev->addr_len) == 0) {
            printk(KERN_ERR "fail to dev_hard_header");
            kfree_skb(skb);
            return -EFAULT;
        }

        p = skb_put(skb, 2);
        *p++ = len / 0x100;
        *p++ = len % 0x100;

        if (copy_from_user(skb_put(skb, len), buf, len)) {
            kfree_skb(skb);
            return -EFAULT;
        }

        if ((ret = dev_queue_xmit(skb)) != NET_XMIT_SUCCESS) {
            printk(KERN_ERR "fail to dev_queue_xmit (%d)", ret);
            kfree_skb(skb);
            return -EFAULT;
        }

        buf += len;
    } while (c > 0);

    *pos += count - c;

    return count - c;
}

static int ose_ioctl(struct inode* inode, struct file *filp,
                     unsigned int cmd, unsigned long arg) {
    // for any command
    if (copy_from_user(daddr, (void __user *) arg, 6)) {
        return -EFAULT;
    }

    return 0;
}

static int ose_recv_frame(struct sk_buff *skb, struct net_device *dev,
                          struct packet_type *pt, struct net_device *orig_dev) {
    int len;

    len = skb->data[0] * 0x100 + skb->data[1];
    skb_pull(skb, 2);
    skb_trim(skb, len);

    printk(KERN_INFO "recieved data size: %d", skb->len);

    spin_lock(&receive_queue_lock);
    skb_queue_tail(&receive_queue, skb); 
    spin_unlock(&receive_queue_lock);

    wake_up_interruptible(&read_queue);

    return 0;
}

static struct file_operations ose_fops = {
    .owner = THIS_MODULE,
    .open = ose_open,
    .release = ose_release,
    .read = ose_read,
    .write = ose_write,
    .ioctl = ose_ioctl
};

static struct packet_type ose_packet_type = {
    .type = htons(ETH_P_X25),
    .func = ose_recv_frame
};

static int ose_init(void) {
    if (register_chrdev(OSE_MAJOR, "ose", &ose_fops)) {
        return -EBUSY;
    }

    spin_lock_init(&lock);
    spin_lock_init(&receive_queue_lock);
    access_ok = 1;

    skb_queue_head_init(&receive_queue);
    init_waitqueue_head(&read_queue);

    dev_add_pack(&ose_packet_type);

    return 0;
}

static void ose_exit(void) {
    unregister_chrdev(OSE_MAJOR, "ose");
    dev_remove_pack(&ose_packet_type);
}

module_init(ose_init);
module_exit(ose_exit);
