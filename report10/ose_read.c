#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

unsigned char addr[6] = {0x52, 0x54, 0x00, 0x12, 0x34, 0x56};

int main(int argc, char *argv[]) {
    int ose;
    int read_size;
    int buf_size = 16;
    char *buf;

    if (argc > 1) {
        sscanf(argv[1], "%d", &buf_size);
    }
    buf = (char *) malloc(buf_size);

    if ((ose = open("/dev/ose", O_RDWR)) < 0) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    if (ioctl(ose, 1, &addr) < 0) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }

    while (1) {
        read_size = read(ose, buf, buf_size);
        if (read_size == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        } else if (read_size == 0) {
            break;
        } else {
            fwrite(buf, 1, read_size, stdout);
        }
    }

    free(buf);
    close(ose);
    
    return 0;
}
