#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char addr[6] = {0x52, 0x54, 0x00, 0x12, 0x34, 0xac};

int main(int argc, char *argv[]) {
    int ose;
    int read_size, write_size;
    int buf_size = 16;
    char *buf;

    if (argc > 1) {
        sscanf(argv[1], "%d", &buf_size);
    }
    buf = (char *) malloc(buf_size);

    if ((ose = open("/dev/ose", O_RDWR)) < 0) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    if (ioctl(ose, 1, &addr) < 0) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }

    while (1) {
        read_size = fread(buf, 1, buf_size, stdin);
        if (read_size == 0) break;
        write_size = write(ose, buf, read_size);
        if (write_size == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }

    free(buf);
    close(ose);
    
    return 0;
}
