#include <stdio.h>
#include <signal.h>
#include "job.h"

void sighandler(int signum) {
    wait_background_jobs();
}

int main(int argc, char *argv[]) {
    struct sigaction sigact;
    sigset_t sigset;

    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = SA_RESTART;
    sigact.sa_handler = sighandler;

    sigaction(SIGCHLD, &sigact, NULL);

    signal(SIGINT, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGCHLD);

    while (1) {
        command *c;
        int bg;

        printf("> ");
        if ((c = read_command(&bg)) != NULL) {
            sigprocmask(SIG_BLOCK, &sigset, NULL);

            update_jobs();
            launch_job(c, bg);
            update_jobs();

            sigprocmask(SIG_UNBLOCK, &sigset, NULL);
        } else {
            sigprocmask(SIG_BLOCK, &sigset, NULL);

            update_jobs();

            sigprocmask(SIG_UNBLOCK, &sigset, NULL);
        }
    }

    return 0;
}
