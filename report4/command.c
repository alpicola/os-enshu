#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "builtin.h"

typedef enum _token {
    T_EOF, T_EOL, T_AND, T_PIPE,
    T_IREDIR, T_OREDIR, T_AREDIR, T_WORD
} token;

extern char **environ;

token read_token(char *buf, int size) {
    token t;
    char c, d;
    int i = 0;
    int delimited = 0;

    while ((c = getchar()) != EOF) {
        if (c != '\t' && c != ' ') break;
    }

    switch (c) {
        case EOF:
            t = T_EOF;
            break;
        case '\n':
            t = T_EOL;
            break;
        case '\'':
            while ((c = getchar()) != EOF) {
                if (c == '\n') printf("quote> ");
                if (c == '\'') break;
                buf[i++] = c;
            }
            t =  T_WORD;
            break;
        case '&':
            buf[i++] = c;
            t = T_AND;
            break;
        case '|':
            buf[i++] = c;
            t = T_PIPE;
            break;
        case '>':
            buf[i++] = c;
            if ((c = getchar()) == '>') {
                buf[i++] = c;
                t = T_AREDIR;
            } else {
                ungetc(c, stdin);
                t = T_OREDIR;
            }
            break;
        case '<':
            buf[i++] = c;
            t = T_IREDIR;
            break;
        default:
            buf[i++] = c;
            t = T_WORD;

            while (!delimited && (c = getchar()) != EOF) {
                switch (c) {
                    case '\t':
                    case ' ':
                        delimited = 1;
                        break;
                    case '\n':
                    case '\'':
                    case '&':
                    case '|':
                    case '<':
                        ungetc(c, stdin);
                        delimited = 1;
                        break;
                    case '>':
                        if (i == 1 && sscanf(buf, "%[012]", &d) == 1) {
                            buf[i++] = c;
                            if ((c = getchar()) == '>') {
                                buf[i++] = c;
                                t = T_OREDIR;
                            } else {
                                ungetc(c, stdin);
                                t = T_AREDIR;
                            }
                        } else {
                            ungetc(c, stdin);
                        }
                        delimited = 1;
                        break;
                    default:
                        buf[i++] = c;
                }
            }
    }

    buf[i] = '\0';

    return t;
}

command *read_command(int *bg) {
    char buf[256];
    token t;
    command *command, *c;
    int fd, err;

    *bg = err = 0;
    command = c = NULL;
    do {
        t = read_token(buf, 256);

        switch (t) {
            case T_EOF:
                command = init_command();
                command->argv[0] = strdup("exit");
                command->argc++;
                break;
            case T_EOL:
                if (c != NULL && c->argc == 0) {
                    printf("pipe> ");
                } else {
                    t = T_EOF;
                }
                break;
            case T_AND:
                if (c != NULL && c->argc > 0 &&
                        read_token(buf, 256) != T_EOF) {
                    *bg = 1;
                    t = T_EOF;
                } else {
                    err = 1;
                }
                break;
            case T_PIPE:
                if (c != NULL && c->argc > 0) {
                    c->next = init_command();
                    c = c->next;
                } else {
                    err = 1;
                }
                break;
            case T_IREDIR:
            case T_OREDIR:
            case T_AREDIR:
                fd = -1;
                sscanf(buf, "%d", &fd);

                if (c != NULL && read_token(buf, 256) == T_WORD) {
                    switch (t) {
                        case T_IREDIR:
                            if (fd < 0) fd = 0;
                            c->redirections[fd].mode = R_READ;
                            break;
                        case T_OREDIR:
                            if (fd < 0) fd = 1;
                            c->redirections[fd].mode = R_WRITE;
                            break;
                        case T_AREDIR:
                            if (fd < 0) fd = 1;
                            c->redirections[fd].mode = R_APPEND;
                            break;
                    }
                    c->redirections[fd].filepath = strdup(buf);
                } else {
                    err = 1;
                }
                break;
            case T_WORD:
                if (c == NULL) {
                    c = init_command();
                    if (command == NULL) {
                        command = c;
                    }
                }
                if (c->argc == ARGV_MAX) {
                    err = 1;
                } else {
                    c->argv[c->argc++] = strdup(buf);
                }
                break;
        }
    } while (t != T_EOF && !err);

    if (err) {
        fgets(buf, 256, stdin);
        fputs("nish: parse error\n", stderr);
        if (command != NULL) {
            free_command(command);
            command = NULL;
        }
    }

    return command;
}

pid_t exec_command(command *c, pid_t pgid, int subshell) {
    int orig;
    int fd[2];
    int i, status;
    pid_t pid;
    char prog[256], *path, *dir;

    if (c->next != NULL) {
        if (pipe(fd) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
    } else if (!subshell && exec_builtin_command(c, 0) != -1) {
        return 0;
    }

    if ((pid = fork()) == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        signal(SIGINT, SIG_DFL);
        signal(SIGTSTP, SIG_DFL);

        if (c->next != NULL) {
            close(fd[0]);
            close(1);
            dup2(fd[1], 1);
            close(fd[1]);
        }

        for (i = 0; i < REDIR_NUM; i++) {
            int f = -1;
            redirection r = c->redirections[i];
            if (r.filepath != NULL) {
                switch (r.mode) {
                    case R_READ:
                        f = open(r.filepath, O_RDONLY);
                        break;
                    case R_WRITE:
                        f = creat(r.filepath, S_IREAD | S_IWRITE);
                        break;
                    case R_APPEND:
                        f = open(r.filepath, O_WRONLY | O_APPEND);
                        break;
                }
                if (f < 0) {
                    perror(r.filepath);
                    exit(EXIT_FAILURE);
                }
                dup2(f, i);
            }
        }

        if ((status = exec_builtin_command(c, 1)) != -1) {
            exit(status);
        } else if (strchr(c->argv[0], '/') == NULL) {
             for (i = 0; environ[i]; i++) {
                if (strncmp(environ[i], "PATH=", 5) == 0) {
                    path = strdup(environ[i] + 5);
                    break;
                }
            }

            while ((dir = strsep(&path, ":")) != NULL) {
                int len = strlen(dir);
                strcpy(prog, dir);
                prog[len] = '/';
                strcpy(prog + len + 1, c->argv[0]);
                execve(prog, c->argv, environ);
            }
        } else {
            execve(c->argv[0], c->argv, environ);
        }

        fprintf(stderr, "nish: cannot run: %s\n", c->argv[0]);
        exit(127);
    }

    if (pgid == 0) {
        setpgid(pid, pid);
        pgid = pid;
    } else {
        setpgid(pid, pgid);
    }
    
    if (c->next != NULL) {
        orig = dup(0);
        close(0);
        dup2(fd[0], 0);
        close(fd[0]);
        close(fd[1]);

        exec_command(c->next, pgid, subshell);

        close(0);
        dup2(orig, 0);
        close(orig);
    }

    return pid;
}

command *init_command() {
    command *c;
    int i;

    c = (command*)malloc(sizeof(command));
    c->next = NULL;

    c->argc = 0;
    for (i = 0; i < ARGV_MAX; i++) {
        c->argv[i] = NULL;
    }

    for (i = 0; i < REDIR_NUM; i++) {
        c->redirections[i].filepath = NULL;
    }

    return c;
}

void free_command(command *c) {
    int i;

    for (i = 0; i < c->argc; i++) {
        free(c->argv[i]);
    }

    for (i = 0; i < REDIR_NUM; i++) {
        if (c->redirections[i].filepath != NULL) {
            free(c->redirections[i].filepath);
        }
    }

    if (c->next != NULL) free_command(c->next);
    free(c);
}
