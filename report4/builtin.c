#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "job.h"

int exec_builtin_command(command *c, int subshell) {
    int i;
    job j;
    char *path;

    if (strcmp(c->argv[0], "exit") == 0) {
        exit(EXIT_SUCCESS);
    }

    if (strcmp(c->argv[0], "cd") == 0) {
        if (c->argc > 1) {
            path = c->argv[1];
        } else {
            path = getenv("HOME");
        }
        if (chdir(path) != -1) {
            return 0;
        } else {
            perror("cd");
            return 1;
        }
    }

    if (strcmp(c->argv[0], "jobs") == 0) {
        if (!subshell) {
            for (i = 0; i < JOBS_MAX; i++) {
                if (jobs[i].status == J_BG) {
                    printf("[%d] running     %s\n", i + 1, jobs[i].command->argv[0]);
                } else if (jobs[i].status == J_ST) {
                    printf("[%d] suspended   %s\n", i + 1, jobs[i].command->argv[0]);
                }
            }
        }
        return 0;
    }

    if (strcmp(c->argv[0], "fg") == 0) {
        if (subshell) {
            fputs("fg: no job control in this shell\n", stderr);
            return 1;
        } else {
            if (c->argc > 1 && sscanf(c->argv[1], "%%%d", &i) > 0) {
                i--;
                if (i < 0 || JOBS_MAX <= i || jobs[i].status == J_INIT) {
                    fprintf(stderr, "fg: no such job: %%%d\n", i + 1);
                    return 1;
                }
            } else {
                for (i = 0; i < JOBS_MAX; i++) {
                    if (jobs[i].status == J_BG || jobs[i].status == J_ST) {
                        break;
                    }
                }
                if (i == JOBS_MAX) {
                    fputs("fg: no job\n", stderr);
                    return 1;
                }
            }

            j = jobs[i];
            jobs[i].command = NULL;
            jobs[i].pgid = 0; 
            jobs[i].status = J_INIT; 

            if (j.status == J_ST) {
                kill(-j.pgid, SIGCONT);
            }
            j.status = J_FG;
            wait_foreground_job(&j);

            return 0;
        }
    }

    if (strcmp(c->argv[0], "bg") == 0) {
        if (subshell) {
            fputs("bg: no job control in this shell\n", stderr);
            return 1;
        } else {
            if (c->argc > 1 && sscanf(c->argv[1], "%%%d", &i) > 0) {
                i--;
                if (i < 0 || JOBS_MAX <= i || jobs[i].status == J_INIT) {
                    fprintf(stderr, "bg: no such job: %%%d\n", i + 1);
                    return 1;
                } else if (jobs[i].status == J_INIT) {
                    fprintf(stderr, "bg: already running job: %%%d\n", i + 1);
                    return 1;
                }
            } else {
                for (i = 0; i < JOBS_MAX; i++) {
                    if (jobs[i].status == J_ST) {
                        break;
                    }
                }
                if (i == JOBS_MAX) {
                    fputs("bg: no suspended job\n", stderr);
                    return 1;
                }
            }

            jobs[i].status = J_BG; 
            printf("[%d] continued   %s\n", i + 1, jobs[i].command->argv[0]);
            kill(-jobs[i].pgid, SIGCONT);
            return 0;
        }
    }

    return -1;
}
