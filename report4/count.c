#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

volatile int count;

void sighandler(int signum) {
    count++;
}

int main(void) {
    struct sigaction sigact;

    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    sigact.sa_handler = sighandler;
    sigact.sa_restorer = NULL;

    count = 0;
    sigaction(SIGINT, &sigact, NULL);

    while (count < 10) {}

    puts("exit");

    return 0;
}
