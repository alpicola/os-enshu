#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "job.h"

void launch_job(command *c, int bg) {
    int i;
    pid_t pgid;
    job j;

    if ((pgid = exec_command(c, 0, bg)) > 0) {
        for (i = 0; i < JOBS_MAX; i++) {
            if (jobs[i].status == J_INIT) break;
        }

        if (i < JOBS_MAX) {
            if (bg) {
                jobs[i].command = c;
                jobs[i].pgid = pgid;
                jobs[i].status = J_BG;
                printf("[%d] %d\n", i + 1, pgid);
            } else {
                j.command = c;
                j.pgid = pgid;
                j.status = J_FG;
                wait_foreground_job(&j); 
            }
        } else {
            fputs("too many jobs\n", stderr);
        }
    }
}

void wait_foreground_job(job *j) {
    int i, status;
    sigset_t sigset, oldsigset;

    if (tcsetpgrp(0, j->pgid) == -1) {
        perror("tcsetpgrp");
        exit(EXIT_FAILURE);
    }

    while (waitpid(-j->pgid, &status, WUNTRACED) > 0) {
        if (WIFSTOPPED(status)) {
            j->status = J_ST;
            break;
        }
    }
    if (j->status == J_ST) {
        for (i = 0; i < JOBS_MAX; i++) {
            if (jobs[i].status == J_INIT) {
                jobs[i].command = j->command;
                jobs[i].pgid = j->pgid;
                jobs[i].status = J_ST;
                break;
            }
        }
        printf("[%d] stopped    %s\n", i + 1, jobs[i].command->argv[0]);
    }

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGTTOU);
    sigprocmask(SIG_BLOCK, &sigset, &oldsigset);
    if (tcsetpgrp(0, getpgrp()) == -1) {
        perror("tcsetpgrp");
        exit(EXIT_FAILURE);
    }
    sigprocmask(SIG_SETMASK, &oldsigset, NULL);
}

void wait_background_jobs() {
    int i, status;
    pid_t pid;

    for (i = 0; i < JOBS_MAX; i++) {
        if (jobs[i].status == J_BG || jobs[i].status == J_ST) {
            while ((pid = waitpid(-jobs[i].pgid, &status, WNOHANG | WUNTRACED)) != 0) {
                if (pid == -1) {
                    jobs[i].status = J_END;
                    break;
                }
                if (WIFSTOPPED(status)) {
                    jobs[i].status = J_ST;
                }
            }
        }
    }
}

void update_jobs() {
    int i;

    for (i = 0; i < JOBS_MAX; i++) {
        if (jobs[i].status == J_END) {
            printf("[%d] done        %s\n", i + 1, jobs[i].command->argv[0]);
            free_command(jobs[i].command);
            jobs[i].command = NULL;
            jobs[i].pgid = -1;
            jobs[i].status = J_INIT;
        }
    }
}
