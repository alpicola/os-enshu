#include <sys/wait.h>

#define ARGV_MAX 32
#define REDIR_NUM 3

typedef enum _redirection_mode {
    R_READ, R_WRITE, R_APPEND
} redirection_mode;

typedef struct _redirection {
    enum _redirection_mode mode;
    char *filepath;
} redirection;

typedef struct _command {
    int argc;
    char *argv[ARGV_MAX];
    struct _redirection redirections[REDIR_NUM];
    struct _command *next;
} command;

command *init_command();
void free_command(command *c);

command *read_command(int *bg);
pid_t exec_command(command *c, pid_t pgid, int subshell);
