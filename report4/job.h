#include <sys/wait.h>
#include "command.h"

#define JOBS_MAX 32

typedef enum _job_status {
    J_INIT, J_FG, J_BG, J_ST, J_END
} job_status;

typedef struct _job {
    pid_t pgid;
    job_status status;
    command *command;
} job;

job jobs[JOBS_MAX];

void launch_job(command *c, int bg);
void wait_foreground_job(job *j);
void wait_background_jobs(); /* non-blocking */
void update_jobs();
